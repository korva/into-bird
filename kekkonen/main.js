var game = new Phaser.Game(800, 600, Phaser.AUTO, 'game_div');

var main_state = {

    preload: function() {
        this.game.stage.backgroundColor = '#ffdddd';
        this.game.load.image('bird', 'assets/bird.png');  
        this.game.load.image('background', "assets/background.png");  


        // Load opponents
        this.game.load.image('flameguy', 'assets/flameguy.png');
        this.game.load.image('carrot', 'assets/carrot.png');
        this.game.load.image('mole', 'assets/mole.png');
        this.game.load.image('guy', 'assets/guy.png');
        this.game.load.image('monster', 'assets/monster.png');
        this.game.load.image('top_flames', 'assets/top_flames.png');
        this.game.load.image('bottom_flames', 'assets/bottom_flames.png');
        this.game.load.image('guy2', 'assets/guy2.png');
        this.game.load.image('car', 'assets/car.png');
        this.game.load.image('heart2', 'assets/heart2.png');
        this.game.load.image('twister', 'assets/twister.png');
        this.game.load.image('raketti', 'assets/raketti.png');
        this.game.load.image('cat', 'assets/cat.png');
        this.game.load.image('kekkonen', 'assets/kekkonen.png');
        this.game.load.image('blue', 'assets/blue.png');
        this.game.load.image('face', 'assets/face.png');
        this.game.load.image('rocks', 'assets/rocks.png');
        this.game.load.image('heart1', 'assets/heart1.png');
        this.game.load.image('fairy2', 'assets/fairy2.png');
        this.game.load.image('kuvio', 'assets/kuvio.png');
        this.game.load.image('thing', 'assets/thing.png');
        this.game.load.image('fairy3', 'assets/fairy3.png');
        this.game.load.image('lady', 'assets/lady.png');

        this.opponents = [
                {
                    numSprites: 1,
                    sprite1Key: 'flameguy',
                    sprite2Key: null 
                },
                {
                    numSprites: 2,
                    sprite1Key: 'carrot',
                    sprite2Key: 'mole'
                },
                {
                    numSprites: 1,
                    sprite1Key: 'guy',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'monster',
                    sprite2Key: null 
                },
                {
                    numSprites: 2,
                    sprite1Key: 'top_flames',
                    sprite2Key: 'bottom_flames'
                },
                {
                    numSprites: 1,
                    sprite1Key: 'guy2',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'car',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'heart2',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'twister',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'raketti',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'cat',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'kekkonen',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'blue',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'face',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'rocks',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'heart1',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'fairy2',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'kuvio',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'thing',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'fairy3',
                    sprite2Key: null 
                },
                {
                    numSprites: 1,
                    sprite1Key: 'lady',
                    sprite2Key: null 
                }
            ];

        // Load sounds
        this.game.load.audio('jump', 'assets/jump.wav');
        this.game.load.audio('end', 'assets/end.wav');
        this.game.load.audio('score', 'assets/score.wav');

        this.game_running = false;
    },

    create: function() {
        var opponentIdx, opponent;

        this.game.physics.startSystem(Phaser.Physics.ARCADE);

        this.backgroundImage = this.game.add.sprite(0, 0, 'background');

        var space_key = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        space_key.onDown.add(this.jump, this);
        this.game.input.onDown.add(this.jump, this);

        this.opponentGroup = game.add.group();
        for (opponentIdx in this.opponents) {
            opponent = this.opponents[opponentIdx];
            opponent.sprite1 = this.opponentGroup.create(0, 0, opponent.sprite1Key, '', false);
            this.game.physics.enable(opponent.sprite1, Phaser.Physics.ARCADE);
            this.shrink_bounding_rectangle(opponent.sprite1);

            if (opponent.numSprites === 2) {
                opponent.sprite2 = this.opponentGroup.create(0, 400, opponent.sprite2Key, '', false);
                this.game.physics.enable(opponent.sprite2, Phaser.Physics.ARCADE);
                this.shrink_bounding_rectangle(opponent.sprite2);
            }
        }        
                   
        this.bird = this.game.add.sprite(100, 245, 'bird');
        this.game.physics.enable(this.bird, Phaser.Physics.ARCADE);
        this.shrink_bounding_rectangle(this.bird);

        
         // Change the anchor point of the bird
        this.bird.anchor.setTo(-0.2, 0.5);
               
        this.score = -1;
        var style = { font: "30px Arial", fill: "#ffffff" };
        this.label_score = this.game.add.text(20, 20, "0", style); 

        // Add sounds to the game
        this.jump_sound = this.game.add.audio('jump');
        this.end_sound = this.game.add.audio('end');
        this.score_sound = this.game.add.audio('score');

    },

    update: function() {
        if (this.bird.inWorld === false)
            this.restart_game(); 

        // Make the bird slowly rotate downward
        if (this.bird.angle < 20)
            this.bird.angle += 1;

        this.game.physics.arcade.overlap(this.bird, this.opponentGroup, this.hit_opponent, null, this);
    },

    jump: function() {
        // if the bird hit an opponent, no jump
        if (this.bird.alive === false)
            return; 

        if (this.game_running === false) {
            this.game_running = true;
            // set timer that adds new opponents 
            this.timer = this.game.time.events.loop(1900, this.add_opponent, this);
            // change gravity to alter bird falling speed
            this.bird.body.gravity.y = 1000; 
        }

        // set jump velocity
        this.bird.body.velocity.y = -350;

        // Animation to rotate the bird
        this.game.add.tween(this.bird).to({angle: -20}, 100).start();

        // Play a jump sound
        this.jump_sound.play();
    },

    // Make the body of the sprite (= bounding rect used for
    // collision testing) to be only the middle part of the sprite,
    // so collisions will not take place too eagerly 
    shrink_bounding_rectangle: function (sprite) {

        var width = sprite.width * 2 / 3;
        var height = sprite.height * 2 / 3;
        var offsetX = sprite.width * 1 / 6;
        var offsetY = sprite.height * 1 / 6;

        sprite.body.setSize(width, height, offsetX, offsetY);
    },

    // Dead animation when the bird hit an opponent
    hit_opponent: function(bird, group) {

        // If the bird has already hit an opponent, we have nothing to do
        if (this.bird.alive === false)
            return;

        // Set the alive flag to false
        this.bird.alive = false;

        // Prevent new opponents from apearing
        this.game.time.events.remove(this.timer);

        this.game.tweens.removeAll();

        // Go trough all the opponents, and stop their movement
        this.opponentGroup.forEachAlive(function (p) {
            p.body.velocity.x = 0;
        }, this);

        this.end_sound.play();
    },

    restart_game: function() {
        if(this.score < 0) this.score = 0;
        window.alert("Sait " + this.score + " pistettä!");

        this.game.time.events.remove(this.timer);
        this.game.state.start('main');
    },

    add_opponent: function() {
        var opponentIdx;
        var sprite1;
        var sprite2;
        var i = 0;

        // Get a random dead opponent
        do {
            opponentIdx = Math.floor(Math.random() * this.opponents.length)
            sprite1 = this.opponents[opponentIdx].sprite1;
            sprite2 = this.opponents[opponentIdx].sprite2;
            i += 1;
        } while (sprite1.alive && i < this.opponents.length);

        // Make sure we really found a dead opponent
        if (sprite1.alive) {
            window.alert("Internal error");
            this.hit_opponent();
        }

        switch (this.opponents[opponentIdx].numSprites) {
            case 1:
                // single-sprite opponents are placed randomly
                sprite1.reset(600, Math.floor(Math.random()*300));

                sprite1.body.velocity.x = -300;
                sprite1.checkWorldBounds = true; 
                sprite1.outOfBoundsKill = true;

                // Animation to wobble the opponent
                this.game.add.tween(sprite1).to({angle: -3}, 100, null, false, 0, Number.MAX_VALUE, true).start();

                break;
            case 2:
                // Two-sprite opponents are placed at the top and bottom
                sprite1.reset(550, 0);
                sprite2.reset(550, 420);

                sprite1.body.velocity.x = -250;
                sprite1.checkWorldBounds = true;
                sprite1.outOfBoundsKill = true;
                sprite2.body.velocity.x = -250;
                sprite2.checkWorldBounds = true;
                sprite2.outOfBoundsKill = true;

                break;
        }
    
        this.score += 1;
        if(this.score > 0) {
            this.label_score.content = this.score;
            this.score_sound.play();
        }
        
    },
};

game.state.add('main', main_state);  
game.state.start('main'); 